import { useState, useEffect } from 'react';
import reactLogo from './assets/react.svg';
import './App.css';

function obterNumeroAleatorio(n1 = 1, n2 = 60) {
  const min = Math.ceil(n1);
  const max = Math.floor(n2);
  return Math.floor(Math.random() * (max - min)) + min;
}

function App() {
  const [dezenas, setDezenas] = useState([]);

  function gerarDezenasDiferentes() {
    const dezenas = [];
    while (dezenas.length < 6) {
      const dezena = obterNumeroAleatorio();
      if (!dezenas.includes(dezena)) {
        dezenas.push(dezena);
      }
    }
    return dezenas.sort((a, b) => a - b);
  }

  useEffect(() => {
    setDezenas(gerarDezenasDiferentes());
  }, []);

  return (
    <div className="App">
      <h1 className="title">Gerador de Dezenas</h1>
      <div className="card">
        <div className="container-dozens">
          {dezenas.map((dezena) => (
            <input
              key={dezena}
              type="text"
              value={dezena}
              readOnly
              className="dozens"
            />
          ))}
        </div>
        {/* <p className="info">Click em cada um dos campos para copiar</p> */}

        <button
          className="buttonGenerate"
          onClick={() => setDezenas(gerarDezenasDiferentes())}
        >
          Gerar Novas Dezenas
        </button>
      </div>
      <p className="read-the-docs">
        <img src="/vite.svg" className="logo" alt="Vite logo" />

        <p>
          Criado por{' '}
          <a href="https://www.linkedin.com/in/brunoardel/" target="_blank">
            Bruno Ardel
          </a>
        </p>

        <img src={reactLogo} className="logo react" alt="React logo" />
      </p>
    </div>
  );
}

export default App;
