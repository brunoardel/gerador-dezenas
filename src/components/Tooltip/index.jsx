import './styles.css';

const Tooltip = ({ children }) => {
  const [isTooltipVisible, setIsTooltipVisible] = useState(false);

  useEffect(() => {
    const timeout = setTimeout(() => {
      setIsTooltipVisible(false);
    }, 3000);

    return () => {
      clearTimeout(timeout);
    };
  }, [isTooltipVisible]);

  return (
    isTooltipVisible && (
      <div className="tooltip">
        {children}
        <p>Copiado!</p>
      </div>
    )
  );
};

export default Tooltip;
